use super::command::{Category, Command};
use super::userdata::UserData;
use futures::{stream::FuturesUnordered, StreamExt};
use std::collections::HashMap;

struct UserStats {
    id: i32,
    preceding: u32,
    during: u32,
    subsequent: u32,
}

pub async fn filter_users(
    command: &Command,
    data_by_user: HashMap<i32, Vec<UserData>>,
) -> Vec<i32> {
    data_by_user
        .into_iter()
        .map(async move |user_and_data| {
            user_and_data.1.into_iter().fold(
                UserStats {
                    id: user_and_data.0,
                    preceding: 0,
                    during: 0,
                    subsequent: 0,
                },
                |mut user_stats, data| {
                    if data.date > command.to_date
                        && data.date <= command.to_date + command.get_span()
                    {
                        user_stats.subsequent += data.meals;
                    } else if data.date < command.from_date
                        && data.date >= command.from_date - command.get_span()
                    {
                        user_stats.preceding += data.meals;
                    } else if data.date >= command.from_date && data.date <= command.to_date {
                        user_stats.during += data.meals;
                    }

                    user_stats
                },
            )
        })
        .collect::<FuturesUnordered<_>>()
        .collect::<Vec<_>>()
        .await
        .into_iter()
        .filter_map(|user_stats| match command.category {
            Category::Bored => get_id_if_valid(|s| s.during < 5 && s.preceding >= 5, user_stats),
            Category::Active => get_id_if_valid(|s| s.during > 5, user_stats),
            Category::SuperActive => get_id_if_valid(|s| s.during > 10, user_stats),
        })
        .collect()
}

fn get_id_if_valid<F>(f: F, stats: UserStats) -> Option<i32>
where
    F: FnOnce(&UserStats) -> bool,
{
    if f(&stats) {
        Some(stats.id)
    } else {
        None
    }
}
